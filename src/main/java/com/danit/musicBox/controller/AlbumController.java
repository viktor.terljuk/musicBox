package com.danit.musicBox.controller;

import com.danit.musicBox.dao.AlbumDao;
import com.danit.musicBox.entity.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AlbumController {

    @Autowired
    private AlbumDao albumDao;

    @RequestMapping(path = "/api/albums", method = RequestMethod.GET)
    public List<Album> gerAll() {
        return albumDao.getAll();
    }

    @RequestMapping(path = "/api/album/{id}", method = RequestMethod.GET)
    public Album getById(@PathVariable int id) {
        return albumDao.getById(id);
    }

    @RequestMapping(path = "/api/album/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable int id) {
        albumDao.deleteById(id);
    }

    @RequestMapping(path = "/api/album/{id}", method = RequestMethod.PUT)
    public void modify(@RequestBody Album album, @PathVariable int id) {
        albumDao.modify(album, id);
    }

    public void save(@RequestBody Album album) {
        albumDao.save(album);
    }

    @RequestMapping(path = "/api/albums/{id}/cover", method = RequestMethod.DELETE)
    public void deleteCover(@PathVariable int id) {
        albumDao.deleteAlbumCover(id);
    }
}
