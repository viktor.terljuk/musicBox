/*
package com.danit.musicBox.controller;

import com.danit.musicBox.dao.AlbumDao;
import com.danit.musicBox.dao.TrackDao;
import com.danit.musicBox.entity.Album;
import com.danit.musicBox.entity.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class TrackUploadController {

    @Autowired
    TrackDao trackDao;

    private static String UPLOADED_FOLDER = "D://Gitrepository//musicBox//src//front-end//public//songs//";
    @GetMapping("http://localhost:3000/admin/addTrack")
    public String index() {
        return "upload";
    }

    @PostMapping("/api/albums/{id}/track")
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
@RequestParam("album") Album album,
 @RequestParam("name") String name,
                                   RedirectAttributes redirectAttributes) throws IOException {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();

            String filePath = UPLOADED_FOLDER + file.getOriginalFilename();
            String pathToFile = "/songs/" +file.getOriginalFilename();
            new File(filePath).createNewFile();
            Path path = Paths.get(filePath);

            Files.write(path, bytes);

            Track track = new Track();
            track.setName(name);
            track.setSongPath(pathToFile);
 track.setAlbum(album);

            trackDao.save(track);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/uploadStatus";
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }
}
*/
