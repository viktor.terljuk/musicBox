package com.danit.musicBox.controller;

import com.danit.musicBox.dao.UserDao;
import com.danit.musicBox.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserDao userDao;

    @RequestMapping(path = "/api/user/{id}", method = RequestMethod.GET)
    public @ResponseBody
    User getById(@PathVariable int id) {
        return userDao.getById(id);
    }

    @RequestMapping(path = "/api/user", method = RequestMethod.GET)
    public @ResponseBody
    List<User> getAll() {
        return userDao.getAll();
    }

    @RequestMapping(value = "/api/me", method = RequestMethod.GET)
    @ResponseBody
    public String currentUserName(Authentication authentication) {
        return "<h3>Username: </h3>" + authentication.getName() +
                "<h3> This user has role: </h3>" +
                authentication.getAuthorities() + "<br>" +
                "<a href = http://localhost:3000/><h4>To the Albums</h4></a>";
    }
}
