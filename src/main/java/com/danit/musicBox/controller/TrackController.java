package com.danit.musicBox.controller;

import com.danit.musicBox.dao.TrackDao;
import com.danit.musicBox.entity.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TrackController {

  @Autowired
  private TrackDao trackDao;

  @RequestMapping(path = "/api/tracks", method = RequestMethod.GET)
  public List<Track> getAll() {
    return trackDao.getAll();
  }

  @RequestMapping(path = "/api/track/{id}", method = RequestMethod.GET)
  public Track getById(@PathVariable int id) {
    return trackDao.getById(id);
  }

  @RequestMapping(path = "/api/track/{id}")
  public void deleteById (@PathVariable int id) {
      trackDao.deleteById(id);
  }

  @RequestMapping(path = "/api/track", method = RequestMethod.PUT)
  public void modify(@RequestBody Track track) {
      trackDao.modify(track);
  }

  @RequestMapping(path = "/api/track", method = RequestMethod.POST)
  public void save(@RequestBody Track track) {
    trackDao.save(track);
  }

}
