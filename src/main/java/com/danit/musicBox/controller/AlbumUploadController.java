package com.danit.musicBox.controller;
import com.danit.musicBox.dao.AlbumDao;
import com.danit.musicBox.entity.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class AlbumUploadController {


    @Autowired
    AlbumDao albumDao;

    private static String UPLOADED_FOLDER = "D://Gitrepository//musicBox//src//front-end//public//covers//";
    @GetMapping("http://localhost:3000/album/create")
    public String index() {
        return "upload";
    }

    @PostMapping("/api/album")
    public String singleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("name") String name,
                                   RedirectAttributes redirectAttributes) throws IOException {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();

            String filePath = UPLOADED_FOLDER + file.getOriginalFilename();
            String pathToFile = "/covers/" +file.getOriginalFilename();
            new File(filePath).createNewFile();
            Path path = Paths.get(filePath);

            Files.write(path, bytes);

            Album album = new Album();
            album.setName(name);
            album.setCoverPath(pathToFile);
            albumDao.save(album);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/uploadStatus";
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }

}