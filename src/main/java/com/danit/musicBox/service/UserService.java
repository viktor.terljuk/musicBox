package com.danit.musicBox.service;

import com.danit.musicBox.entity.User;

public interface UserService {

    void save(User user);

    User findByLogin(String login);
}
