/*
package com.danit.musicBox.service;

import com.danit.musicBox.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class DanITUserDetailsService implements UserDetailsService {

    private final JdbcTemplate jdbc;
    private final String SQL = "select u.login as username, " +
            " u.password as password, r.role as role " +
            " from user u " +
            " inner join user_roles ur on(u.user_id=ur.user_id) " +
            " inner join roles r " +
            " on(ur.role_id=r.role_id) " +
            " where u.username=?;";
    @Autowired
    public DanITUserDetailsService(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = new User();
        jdbc.query(SQL, new Object[]{username}, (rs, rowNum) ->
                user.setLogin(rs.getString("username"))
                        .setPassword(rs.getString("password"))
                        .setRoles(rs.getString("role")));
        System.out.println(user);
        return User
                .withUsername(user.getLogin())
                .password(user.getPassword())
                .authorities(user.getRoles())
                .build();
    }
}
*/
