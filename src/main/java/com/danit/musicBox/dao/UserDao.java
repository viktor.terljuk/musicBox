package com.danit.musicBox.dao;

import com.danit.musicBox.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserDao {

    List<User> getAll();

    User getById (int id);

    User getByLogin(String login);

    User save(User user);
}
