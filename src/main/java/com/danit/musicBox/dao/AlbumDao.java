package com.danit.musicBox.dao;

import com.danit.musicBox.entity.Album;

import java.util.List;

public interface AlbumDao {

    List<Album> getAll();

    Album getById (int id);

    void deleteById (int id);

    Album retriveById (int id);

    void save (Album album);

    void modify (Album album, int id);

    void deleteAlbumCover(int id);


}
