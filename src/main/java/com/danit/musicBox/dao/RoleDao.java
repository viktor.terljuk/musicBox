package com.danit.musicBox.dao;

import com.danit.musicBox.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role,Long> {
}
