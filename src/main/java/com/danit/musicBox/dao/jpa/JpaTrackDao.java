package com.danit.musicBox.dao.jpa;


import com.danit.musicBox.dao.TrackDao;
import com.danit.musicBox.entity.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class JpaTrackDao implements TrackDao {

  private final EntityManager entityManager;

  @Autowired
  public JpaTrackDao(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  @Override
  public List<Track> getAll() {
    Query nativeQuery = entityManager.createNativeQuery(
        "SELECT * FROM track", Track.class
    );
    List resultList = nativeQuery.getResultList();
    return resultList;
  }

  @Override
  public Track getById(int id) {
    Track track = entityManager.find(Track.class, id);
    return track;
  }

  @Override
  public void deleteById(int id) {
    Track track = getById(id);
    entityManager.remove(track);
  }

    @Override
    @Transactional
    public void save(Track track) {
      entityManager.persist(track);
    }

    @Override
    public void modify(Track track) {
        entityManager.persist(track);
    }


}
