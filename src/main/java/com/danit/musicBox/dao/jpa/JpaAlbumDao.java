package com.danit.musicBox.dao.jpa;


import com.danit.musicBox.dao.AlbumDao;
import com.danit.musicBox.entity.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class JpaAlbumDao implements AlbumDao {

    private final EntityManager entityManager;

    @Autowired
    public JpaAlbumDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Album> getAll() {
        Query nativeQuery = entityManager.createNativeQuery(
                "SELECT * FROM album", Album.class
        );
        List resultList = nativeQuery.getResultList();
        return resultList;
    }

    @Override
    public Album getById(int id) {
        Album album = entityManager.find(Album.class, id);
        return album;
    }

    @Override
    public void deleteById(int id) {
        Album album = getById(id);
        entityManager.remove(album);
    }

    @Override
    public Album retriveById(int id) {
        return null;
    }

    @Override
    public void save(Album album) {
        entityManager.persist(album);
    }

    @Override
    public void modify(Album album, int id) {
        Album old = getById(id);
        if (old != null) {
            old.setName(album.getName());
            old.setCoverPath(album.getCoverPath());
            entityManager.merge(old);
        } else {
            throw new NullPointerException("There is no album with that id = " + id);
        }
    }

    @Override
    public void deleteAlbumCover(int id) {
        Query nativeQuery = entityManager.createNativeQuery("UPDATE album SET cover_path = NULL WHERE id =: id");
        nativeQuery.executeUpdate();
    }
}
