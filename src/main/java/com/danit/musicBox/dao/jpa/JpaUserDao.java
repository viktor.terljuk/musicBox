package com.danit.musicBox.dao.jpa;

import com.danit.musicBox.dao.UserDao;
import com.danit.musicBox.entity.User;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;


@Repository
public class JpaUserDao implements UserDao {

    private final EntityManager entityManager;

    public JpaUserDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<User> getAll() {
        Query nativeQuery = entityManager.createNativeQuery(
                "SELECT * FROM user", User.class
        );
        List resultList = nativeQuery.getResultList();
        return resultList;
    }

    @Override
    public User getById(int id) {
       User user = entityManager.find(User.class, id);
        return user;
    }

    @Override
    public User getByLogin(String login) {
        NativeQuery query = (NativeQuery) entityManager.createNativeQuery("SELECT * FROM user u WHERE u.login = :login");
        query.setParameter("login", login);
        query.addEntity(User.class);
        List<User> users = query.getResultList();
        return users.isEmpty() ? null : users.get(0);
    }

    @Override
    public User save(User user) {
        return null;
    }
}
