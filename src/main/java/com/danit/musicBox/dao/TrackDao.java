package com.danit.musicBox.dao;

import com.danit.musicBox.entity.Track;

import java.util.List;

public interface TrackDao {

    List<Track> getAll ();

    Track getById (int id);

    void deleteById (int id);

    void save (Track track);

    void modify (Track track);
}
