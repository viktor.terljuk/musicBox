package com.danit.musicBox.entity;

public enum UserRole {
    USER, ADMIN
}
