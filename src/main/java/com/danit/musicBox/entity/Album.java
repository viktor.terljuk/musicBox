package com.danit.musicBox.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "album")
public class Album {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private int id;

    @Column(name = "name", length = 200, nullable = false)
    private String name;

    @Column(name = "cover_path", length = 400)
    private String coverPath;


    @OneToMany (mappedBy = "album")
    List<Track> trackList;

    public List<Track> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<Track> trackList) {
        this.trackList = trackList;
    }

    public Album() {
    }

    public Album(int id, String name, String coverPath, List<Track> trackList) {
        this.id = id;
        this.name = name;
        this.coverPath = coverPath;
        this.trackList = trackList;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", coverPath='" + coverPath + '\'' +
                '}';
    }
}


