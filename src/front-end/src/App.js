import React, {Component} from "react";
import './index.css'
import Header from './components/Header'
import './components/mainPageStyle.css'
import Admin from './components/Admin'
import AudioPlayer from './components/AudioPlayer';
import User from './components/User'
import {connect} from 'react-redux'
import {loadAlbums} from "./actions/albumActions";
import {adminLoadTracks} from "./actions/trackActions";
import {loadUsers} from "./actions/usersActions";


class App extends Component {
    componentDidMount(){
        if (this.props.albums.length === 0) {
            this.props.loadData();
            this.props.loadTracks();
            this.props.loadUsers();
        }
    }
    render() {
        return (
            <div>
                <Header/>
                <AudioPlayer/>
                <p>{this.props.albums.length}</p>
                <User/>
                <Admin/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    albums: state.albums,
    tracks: state.tracks,
    users: state.users
});

const mapDispatchToProps = dispatch => ({
    loadData: () => dispatch(loadAlbums()),
    loadTracks: () => dispatch(adminLoadTracks()),
    loadUsers: () => dispatch(loadUsers())
});

export default connect(mapStateToProps, mapDispatchToProps) (App)