import {TRACKS_LOADED} from "./types";

export const adminLoadTracks = () => dispatch => {
    fetch('http://localhost:8080/api/tracks')
        .then(res => res.json())
        .then(data => dispatch({type:TRACKS_LOADED, payload: data}))
};
