import {USERS_LOADED} from "./types";

export const loadUsers = () => dispatch => {
    fetch('http://localhost:8080/api/me')
        .then(res => {console.log(res.json())})
        .then(data => dispatch({type:USERS_LOADED, payload: data}))
};
