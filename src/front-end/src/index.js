import React from 'react';
import {Provider} from 'react-redux'
import {BrowserRouter, Route} from 'react-router-dom'
import App from './App';
import store from './store'
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-grid.css';
import { render } from 'react-dom'


render(
    <Provider store={store}>
        <BrowserRouter>
            <Route path={'/'} component={App}/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('main')
);
