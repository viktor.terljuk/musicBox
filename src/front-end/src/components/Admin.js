import React, {Component} from 'react'
import AdminAlbums from './AdminAlbums'
import AdminAlbumsForm from './AdminAlbumsForm'
import AdminTracks from './AdminTracks'
import AdminTracksForm from './AdminTracksForm'
import Route from "react-router-dom/es/Route";
import Switch from "react-router-dom/es/Switch";
import NavLink from "react-router-dom/es/NavLink";
import AdminAlbumPage from "./AdminAlbumPage";

class Admin extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <nav className='nav navbar-dark bg-dark p-2 mb-3' >
                            <NavLink className='navbar-brand' to='/'>Admin</NavLink>
                            <NavLink className='nav-link' to='/'>Home</NavLink>
                            <NavLink className='nav-link' activeClassName='active' to='/album'>Albums</NavLink>
                            <NavLink className='nav-link' activeClassName='active' to='/tracks'>Tracks</NavLink>
                        </nav>
                        <Switch>
                            <Route exact path='/album' component={AdminAlbums}/>
                            <Route exact path='/album/create' component={AdminAlbumsForm}/>
                            <Route exact path='/album/:id' component={AdminAlbumsForm}/>
                            <Route exact path='/admin/albums/:id/tracks/' component={AdminAlbumPage}/>
                            <Route exact path='/admin/addTrack' component={AdminTracksForm}/>
                            <Route exact path='/album/:id/addtrack' component={AdminTracksForm}/>
                        </Switch>
                    </div>
                </div>
            </div>
        )
    }
}

export default Admin