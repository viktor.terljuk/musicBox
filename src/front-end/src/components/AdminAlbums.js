import React, {Component} from 'react'
import './albums.css'
import connect from "react-redux/es/connect/connect";

class AdminAlbums extends Component {

    render() {
        const albumsList = this.props.albums;

        const albumsL = albumsList.map((album) => {
            return (
                <div className="row">
                    <div className="col-3">
                        <img src={album.coverPath} height={'250px'} width={'250px'}/>
                    </div>
                    <div className="col">
                        <a href={'/admin/albums/' + album.id + '/tracks'}><h2>{album.name}</h2></a>
                    </div>
                    <div className="col">
                        <a href={'/album/create'}>
                            <button className="btn-info">
                                Edit album
                            </button>
                        </a>
                    </div>
                    <div className="col">
                        <a href={'/album/create'}>
                            <button className="btn-info">
                                Add new album
                            </button>
                        </a>
                    </div>
                </div>
            )
        });
        return (
            <div className="container">
                {albumsL}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        albums: state.albums
    }
}

export default connect(mapStateToProps)(AdminAlbums)