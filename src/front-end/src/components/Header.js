import React, {Component} from 'react'
import './mainPageStyle.css'
import connect from "react-redux/es/connect/connect";


class Header extends Component {

    render() {

        const user = this.props.user;

        function loggedIn() {
            if (user.role === "USER_ADMIN" || user.role === "USER_USER") {
                return userName;
            } else {
                return loginForm;
            }
        }

        const userName = (
            <div className="header__search">
                <h3>Logged in</h3>
                {/*
                <h3>{user.login}</h3>
*/}
            </div>
        );

        function doNotRedirect(event) {
           return event.preventDefault();
        }

        const loginForm = (
            <div className="header__search">
                <form method="POST" action="http://localhost:8080/login">
                    <input type="text" name="username" className="header__search" placeholder="login"/>
                    <input type="text" name="password" className="header__search" placeholder="password"/>
                    <a href={'/album'}>
                        <button>Submit</button>
                    </a>
                </form>
            </div>
        );

        const headerLogos = (
            <div className="header__logo-icon">
                <a href={'/'} className="header__logo-img">
                    <img
                        src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxATEA8TExAVEBUXFQ8XFRUXDw8PEhUSFREWFhUXGBUYHSggGBslGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEBAQADAQEBAAAAAAAAAAAAAQgCBgcFBAP/xABOEAACAQIEAwQFBQkLDQAAAAAAAQIDMQQRIWEFcbESQVHxBgcIE4EUIlKRoTJicnSCosHC8CMlQkNkc5KTsrPRFSQzNVNjg4SjpMPS4f/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD28Z+AfgTZeQFb7kG+7vJbRC27AreW7DeRLbsW1dwLnlcZ97Ju/IbsCp970CZL8hfl1AqefIZ58iX5DZAXPwDfcibLyFtEBW+5Bv4slt2LbsDz31zem1Th+Fp06EuziK7moyyz93Tjl25pfSzkks/FvuM5YTjmLp4j5RDE1Y1s03V95Jzf4Tb+ctNU80zv/tDV5S4rTi3pDDUllnom6lST0+K+pHmAGtvVt6XriWBhXeUasX7utFWVRJPOK8JJprwza7jtSfe9DP8A7N2PaxmNod06Eau3apVFHpW+w9/vq7AVP4BPPkS/LqL8uoFTz5DPwJfRDZeQFb7kG+5EtohbdgVv4suZxtuypZXuBSkKBxb7kS2iK34XJbdgLbsW3YtuxbV3AW1dxu/IbvyG7AbsX5HXPTP02wfDYU54lzbm37unTip1J9nLtPJtJJZq7Vz+Xof6fYDiTlGhUlGpFdqVGpD3dXs55Z6NxkrWbyzWdwO0X5dRfkL8hsgGyGy8hsvIW0QC2iFt2LbsW3YC27FtXcW1dxuwM1e0BF/5Y54eg/tmv0Hmp6Z7Qb/fdfi1D+1UPMwPUfZ2X77VvxSt/fUTR1+XUzl7Osf31rfilb++oGjb8uoC/LqL6IX0Q2XkA2XkLaIW0QtuwFt2LbsW3Ytq7gLau5Uu9k3ZUu9gUpMygcW8uZLbsreRLau/7aALau43fkN35DdgF4sX5C/IX5dQM7e0bVb4nhlm8lhKbS7k3iK6b+PZX1HQfRDjMsHjsLiU8vd1IuW9N/NqL4wcl8T0v2ksMlisBVy+6o1YZ/zdRS/8r+s8cA29GSaXZemS1Vsn4F2XkdX9WHFflPCMBPPOSpKnJvV9qi3Sbe77GfxO0W0QC2iFt2LbsW3YC27FtXf9tBbV3G7AbvyG7G7CWfIDLvrwxnvON4pfQjh4L+pjJ/bJnQj7vpzjvfcT4hUv2sRXy/BjNxj9iR8ID0v2fqqXF8m/u8PXj+dCX6ppTZGWPUrUceOYH775RF8nh6n+BqfZeQDZeQtohbRC27AW3Ytuxbdi2rv+2gC2ruN2N2N2A3ZVrqS+rsVa8uoHLMAAcXpqTd+RX4sm7AbsX5C/IX5dQF+XUX5C/IbIDx/2kcKnhMDVy+4rThn4KpT7WX/TR4Aaf9euFU+C4h5Z+7nh58v3VQ6TZmADQns5cT7eBxWHzzdKspJeEKsNPzoT+s9btuzOns78ScOJVqOelahLJeNSnKMo/muoaLtuwFt2Lau4tq7jdgN35Ddjdi+rsAvq7B68uovy6iWufh1AxJXquUpTd5Sk3zbzP5nOtDsykvByX1PI4Ad59SlBy45gmv4PyiT5LD1F+lGpraIzd7PNLtcWqP6OFrSW2dSlH9Y0jbdgLbsW3YtuxbV3AW1dxuxuxuwG7F9XYX1dhfl1AX5dS558iX5dS5+AHIEyKBxa72S/IrRL8uoC/LqL8hfkNkA2Q2XkNl5C2iA696w8N7zhPEoJZv5NiJZX1hBzXxzijIJtnHUFOlVp37cJx/pRa/SYpq05RlKMlk4tprwaeTQHYvVvxL5Pxbh9Xu99CD/Bq5039kzXVtXcxDGTTTTya1TTyaa78zZfovxaOKwWFxK/jaVOTV8ptfPj8JKS+AH092N2Z59aXrRxrxtfD4Su8PRoylTcqeSnUqR0nJzyzSUs0svDPv06nwX1k8Xw1SMljatdZpuFepPEQkvB9ttr4NAay3ei/bVnn/pF64OE4aq6XbqYpp5S9xCFSCa7nOUop/k5nw/WJ6bxxPo2sRRbp/KKlKhOKlnKE/nSq02/DKm1vGW5nwDX/on6Z4HiMZPDVc3FJzpSXu6sU/GPet02tzsDfcjF3BOLV8JXp4ihN06kHnFrv8U13xa0a78zU3o76cYfF8LnjY5Q93TqSrwzzdKdODlKO6d0+9Nd+gGUcX/pKj++n/aZ/EAD1X2cv9Z4l/yOovrxFA0VbdmdPZzl++mI/E6un/MUDRdtXcBbV3G7G7G7AbsX1dhfV2F+XUBfl1F+XUX5dRfRAL6IufcibLyLsgLkUhQOLWfIl+RXryJsgGyGy8hsvIW0QC2iFt2LbsW3YC27MmetXhXybjGOppZRlU97HwyrRVR5bJykvgaztq7me/aOwDjjsJW7qtBx/KpVG39lSIHkh7V6kvTB08DxLDSecsPRr4mgn9FQbqR+Euy/y34Hip9L0f4n8nr9vVxlTr0ppd9OtSlTlzy7WfNID50ptttttvVtvNtu+pAAPr4XijWBxOFk32XVoV6a7lVgpU5fXCrn/wANHyAAB+zB8Ur0qWIpQqyhTrRhGrFPSahLtRT5Pq13s/GAAAA9O9nmso8WmvpYWulv+6Upfqmkd2ZE9XXG3g+J4Osk5JT7EorLOUKicJZZ9/zs1ukav4TxShiaUa9GrGrTlZrufepJ6xkrOLyaA/ZuxfV2F9XYX5dQF+XUX5dRfl1F9EAvohsvIbLyGyAbIq007yW0WrKtOYHIEKBxfgTZeRW+5EtogFtELbsW3YtuwFt2Lau4tq7jdgN35HmXr/4K63DI14r52HqRm9M37qfzJ/a4PlFnpu7PzcSwUK9GtSqLOFSE4SXjGUWn1AxSD9PEsHKjWrUZfdU6lSnL8KE3F/aj8wAAAAAAAAAAADtmK41i8LVpY3CV50PlMfeS7DfYeIi+zXhKDzi/3TOSTTyjUidTPucAl76FTBSetR9vDt/wcVFZKN9FUj+5v773bf3IHrHoN67HOpTocRjGCk0liILsRT7vewsl98skvDLU9rzz5dTEBq71Q8VnieD4OU3nKCnSk882/dTcY/HsKIHcb6IbLyGy8hsgGyFtFcW0VxbdgLbsqWV7ktq7lS72BSkKBxb7kS27K39ZLbsBbdi2ruLau43YDd+Q3Y3Yvq7AL6uwvy6i/LqL8uoGSfWhQUOMcSS7685fGaU39smdWO+eu/Ce743i33Tjh5x5OjCL/OjI6GAAAAAAAAAAAAsJNNNNppppp5NNWefiQAfq4rXU69eorTqVZLlKba6mgvZ1ruXC68Po4qr8Iyo0n17RnM909mvHNw4jR8JUKi/KU4y/sxA9r2Qtori2iuLbsBbdi2ruLau43YDdlS72Td+RVrqBcykzKBxbyJbV3K9NSbvyAbvyG7G7F9XYBfV2F+XUX5dRfl1AX5dRfRC+iGy8gM9e0dhVHiGEqL+Fh+y/yKs//c8lPYfaSa+V4BeFGo/rqf8Aw8eAAAAAAAAAAAAAAB6n7O+MceKVqfdUw1TT76FSDX2dr6zyw9a9nbhU546vicn7ulScM8tHUqyWST2jGTfNeIGhbbsW1dxbV3G7AbsbvyG78hfVgL6sq15dSX5dS558uoHIAAcX4sm7K13sl9XYBfV2F+XUX5dRfl1AX5C+iGyGy8gGy8hbRC2iFt2Bn/2kYf57gn/J5fZVl/ieQHuPtKYJ5cNrd3+cwk937uUekjw4AAAAAAAAAAAAAA/vgMHUrVadKlFznOUYQiruUnkka69B/Rmnw7BUcPHJyXzqs0sveVpJdqX2JLaKPM/UD6GdmL4lWhq+1DCprVRtOquesVt2vFHtW7AbsbvyG78hfVgL6sX5dRfl1F+XUBfl1Ln4EvorFz7kByyBMigcWiX5dStZ8iX5dQF+XUX0QvohsvIBsvIW0QtohbdgLbsW3YtuxbV3A869fXC/e8HnU1cqNWjV0WfzW3SkuWVTP8kzKbQ4/wAMWJwuJoS/jaVWny7UWk/g8n8DGVWnKMpQknGUW1JO6knk0/iBwAAAAAAAAAAA7N6vPRWfEcdToLNUl8+tJfwaSazSf0paRW7z7jrJqj1R+iC4fgIuccsRW7NStmvnR0+ZT/JTfxcgO5YXDQpU4QhFQhCMYwillGMIrKMUuSP67vyG78hfVgL6sX5dRfl1F+XUBfl1F9FYX0VhsgGyLsibIttAKUhQOLWfIl9EV+BNl5ANl5C2iFtELbsBbdi27Ft2Lau4C2ruN2N2N2A3ZlD1t8L+T8YxsUso1JqtHlWXbly+e5fUavvq7GZPXzjY1OM1VH+LpUKb/C7LqdKiA87AAAAAAAAAAHe/Uz6OLGcTpucc6WHXvp6PJyi0qcfjNp5d6izUm78jyj2dOFqGAxGIa+dVrOKf+7pRSX50pnq99WAvqxfl1F+XUX5dQF+XUX0VhfRWGyAbIbIbIW0VwFtFcq05ktuyrTmBSkKBxb7kS2iK33Ilt2Atuxbdi27FtXcBbV3/AG0G7G7G7AbsX1dhfV2F+XUBfl1Md+m+LdXiXEKjebeJxOW0VVlGK+EUl8DYl+XUxn6Txyx2NXhiMSvqqyA+YAAAAAAAAAANS+pCllwPBN97xL/7ios/sO9X5dTqXqmw/Z4Lw5P/AGTl/TqSn+sdtvy6gL8uovorC+isNkA2Q2Q2QtorgLaK4tuxbdi27AW3ZUu9ktqypd7ApQAOLf1ktuzkyJZa94Etq7jdlS72Eu9gTdi+rsXLO4yz5dQJfl1F+XUr15B+AEzz0RjX0sy/yhj/AMZxWX9dI2XsjFXGK6qYnET+lVqy/pTb/SB+QAAAA2AAAAAsIttJLNtpJbsDYfoRh3DhnDoNZdnC4VS/C9zHtfbmfbvorH88NS7NOnBWjGMfgkkf1fggJshsi7IWsBLaK4tuy5ZbsJZbsCW3Ytqypd7CXewJu/Iq11Yyz1YvyAuZQAICgCBlABgAARFAEMVRAAoAAAAAAAB/fh3+mofztH+8iUAbNIigCAoAgKAIUACMoAEAAH//2Q=="
                        alt=""
                        className="header__logo-img-icon"/>
                </a>
                <a href={'/'} className="header__logo-text">
                    <img
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTK68ODzHA4rI0eIrVwPwxpRgUlLcLGNsm-_LxX1Sp7-fNwojjdMg"
                        alt=""
                        className="header__logo-img-text"/>
                </a>
            </div>
        );

        const headerLogOut = (
            <div className="header__nav-icon">
                <form method="POST" action="http://localhost:8080/logout">
                <a href="http://localhost:8080/login" className="">
                    <button className="btn-danger">Logout</button>
                </a>
                </form>
            </div>
        );
        return (
            <div className="header">
                {headerLogos}
                {loginForm}
                {headerLogOut}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return ({
        user: state.users
    });
}


export default connect(mapStateToProps)(Header);