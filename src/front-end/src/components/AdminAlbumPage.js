import React, {Component} from 'react'
import {Navbar, Jumbotron, Button} from 'react-bootstrap';
import connect from "react-redux/es/connect/connect";

class AlbumPage extends Component {
    render() {
        const albumTracks = this.props.album.trackList;
        const tracks = albumTracks ? albumTracks.map((track) => {
            return (
                <ul>
                    <img src={this.props.album.coverPath} width={"250px"} height={"250px"}/>
                    <h2>{this.props.album.name}</h2>
                    <div className="col-9">
                        <h4>{track.name}</h4>
                    </div>
                    <div className="col">
                        <audio controls>
                            <source src={track.songPath}/>
                        </audio>
                        <button className="btn-danger">
                            Delete
                        </button>
                        <button className="btn-info">
                            Update
                        </button>
                    </div>
                </ul>
            )
        }) : 'loading...';

        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <ol>
                            {tracks}
                        </ol>
                    </div>
                    <div className="col">
                        <a href={'/album/'+ this.props.album.id +'/addTrack'}><button className="btn-success" >Add 1track</button></a>
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state, ownProps) {

    function findAlbum(album) {
        return album.id === +ownProps.match.params.id
    }

    return {
        album: state.albums.find(findAlbum) || {}
    }

}

export default connect(mapStateToProps)(AlbumPage)