import React, {Component} from 'react'
import {Navbar, Jumbotron, Button} from 'react-bootstrap';

class AdminAlbumsForm extends Component {



    render() {

        // let xhr = new XMLHttpRequest();
        // xhr.open('POST', 'http://localhost:8080/api/album');
        // let body = 'name=' + encodeURIComponent(name) +
        //     '&coverPath=' + encodeURIComponent(coverPath);


        return (
            <div>
                <form method="POST" action="http://localhost:8080/api/album" encType="multipart/form-data">
                    <div className="form-group">
                        <label form="exampleInputEmail1">Album name</label>
                        <input type="text" name="name" id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Enter album Name"/>
                    </div>
                    <div className="form-group">
                        <label form="exampleInputFile">Cover</label>
                        <input type="file" name="file" className="form-control-file" id="exampleInputFile"
                               aria-describedby="fileHelp"/>
                        <small id="fileHelp" className="form-text text-muted">Chose image file on your device
                        </small>
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>

        )
    }
}

export default AdminAlbumsForm
