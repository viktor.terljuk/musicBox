import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect";

class AdminTracks extends Component {
    render() {

        const trackList = this.props.tracks;

        const tracks = trackList.map((track) => {
            return (
                <ul>
                    <h2>Track</h2>
                    <div className="col-9">
                        <img src={track.album.coverPath} height={'300px'} width={'300px'} />
                        <h4>{track.name}</h4>
                    </div>
                    <div className="col">
                        <audio controls>
                            <source src={track.songPath}/>
                        </audio>
                        <button className="btn-danger">
                            Delete
                        </button>
                        <button className="btn-info">
                            Update
                        </button>
                    </div>
                </ul>)
        });

        return (
        <div className="container">
                <div className="row">
                    <a href={'/album/:id/addTrack'}>
                    <button className="btn-info">
                        Add track
                    </button>
                    </a>
                </div>
            <div className="row">
                        {tracks}
                </div>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        tracks: state.tracks,
        albums: state.albums
    }
}
export default connect(mapStateToProps) (AdminTracks)