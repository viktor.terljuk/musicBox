import React, {Component} from 'react'
import {Navbar, Jumbotron, Button} from 'react-bootstrap';
import connect from "react-redux/es/connect/connect";
import state from "../reducers";

class AdminTracksForm extends Component {


    render() {

        const currentAlbum = this.props.album;
        /*const albumId = currentAlbum.map((album) => {
           return album.id;
        });*/

        return (
            <div>
                <form form method="POST" action="http://localhost:8080/api/track" encType="multipart/form-data">
                    <div className="form-group">
                        <label form="exampleInputEmail1">Track name</label>
                        <input type="text" name="name" className="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Enter track Name"/>
                    </div>
                    <div className="form-group">
                        <label form="exampleInputEmail1">Album</label>
                        <input type="email" name="album" className="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="" value={currentAlbum}/>
                    </div>
                    <div className="form-group">
                        <label form="exampleSelect1">Select genre</label>
                        <select className="form-control" id="exampleSelect1">
                            <option>Rock</option>
                            <option>Classic</option>
                            <option>Pop</option>
                            <option>Rap (do not do this!)</option>
                            <option>Another</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label form="exampleInputFile">Music file</label>
                        <input type="file" name="file" className="form-control-file" id="exampleInputFile"
                               aria-describedby="fileHelp"/>
                        <small id="fileHelp" className="form-text text-muted">Chose music file on your device to upload
                        </small>
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>

        )
    }
}

function mapStateToProps(state, owmProps) {

    function findAlbum(album) {
        return album.id === +owmProps.match.params.id
    }

    return {
        album: state.albums.find(findAlbum)
    }
}

export default connect(mapStateToProps)(AdminTracksForm)