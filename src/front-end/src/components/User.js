import React, {Component} from "react";
import {Switch, Route} from 'react-router-dom'
import Albums from './Albums'
import MainPage from './MainPage'
import Admin from './Admin'
import NavLink from "react-router-dom/es/NavLink";
import AlbumPage from './AlbumPage';
import UserAlbums from "./UserAlbums";


class User extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <nav className='nav navbar-dark bg-dark p-2 mb-3'>
                            <NavLink className='navbar-brand' to='/'>User</NavLink>
                            <NavLink className='nav-link' to='/'>Home</NavLink>
                            <NavLink className='nav-link' activeClassName='active' to='/liked'>Liked</NavLink>
                            <NavLink className='nav-link' activeClassName='active' to='/admin'>Admin</NavLink>
                        </nav>
                        <Switch>
                            <Route exact path='/' component={UserAlbums}/>
                            <Route exact path='/album' component={UserAlbums}/>
                            <Route exact path='/albums/:id/tracks' component={AlbumPage}/>
                            <Route exact path='/user/:id/me' component={AlbumPage}/>
                            <Route exact path='/admin' component={Admin}/>
                        </Switch>
                    </div>
                </div>
            </div>
        )
    }
}

export default User