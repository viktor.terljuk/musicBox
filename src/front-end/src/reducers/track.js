const initialState = [];

function trackReducer (state = initialState, action){
    switch(action.type) {
        case 'TRACKS_LOADED':
            return [...action.payload];
        default:
            return state;
    }
}

export default trackReducer