import {combineReducers} from 'redux';
import albumsReducers from './album'
import tracksReducers from './track'

const allReducers = combineReducers({
    albums: albumsReducers,
    tracks: tracksReducers
});

export default allReducers